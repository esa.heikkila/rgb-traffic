const traffic=require('./traffic-light');


console.log("Hexnumber of green: ",traffic.getTrafficHEXColor("GREEN"));
console.log("Hexnumber of yellow: ",traffic.getTrafficHEXColor("YELLOW"));
console.log("Hexnumber of red: ",traffic.getTrafficHEXColor("RED"));
console.log("Colour is not in traffic-lights : ",traffic.getTrafficHEXColor("BLACK"));